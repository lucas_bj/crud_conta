DROP TABLE conta IF EXISTS;


CREATE TABLE conta (
  id         INTEGER IDENTITY PRIMARY KEY,
  agencia  VARCHAR(4),
  numero VARCHAR(6),
  cpf   VARCHAR(11),
  status boolean,
  data_criacao DATE,
  data_atualizacao DATE
);