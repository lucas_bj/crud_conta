# CrudConta

Api que simula uma criação de contas bancárias, com os seguintes endpoints:
        GET /conta
            Lista todas contas cadastradas ativas
        GET /conta/{id}
            Retorna uma conta específica pelo ID
        GET /conta/cpf/{cpf}
            Retorna uma conta específicar pelo CPF
        POST /conta
            Salva uma conta na base de dados
        PUT /conta/{id}
            Atualiza um conta
        DELETE /conta/{id}
            Faz o delete lógico da conta (status = false)

a api possui swagger, podendo ser acessado pelo link <a href="http://localhost:8080/swagger-ui.html#/">SWAGGER</a>

exemplo Json para post:
{
    "agencia":"0100",
	"numero":"351121",
	"cpf":"02721739085"
}