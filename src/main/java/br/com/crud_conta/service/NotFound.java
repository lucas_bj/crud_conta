package br.com.crud_conta.service;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFound extends Exception {
  public NotFound() {
  }

  public NotFound(String s) {
    super(s);
  }

  public NotFound(String s, Throwable throwable) {
    super(s, throwable);
  }

  public NotFound(Throwable throwable) {
    super(throwable);
  }

  public NotFound(String s, Throwable throwable, boolean b, boolean b1) {
    super(s, throwable, b, b1);
  }
}
