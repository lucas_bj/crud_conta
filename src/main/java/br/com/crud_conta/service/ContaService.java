package br.com.crud_conta.service;

import br.com.crud_conta.domain.Conta;
import br.com.crud_conta.model.ContaRequest;
import br.com.crud_conta.model.ContaResponse;
import br.com.crud_conta.repository.ContaRepository;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;


@Service
public class ContaService {

  private final ContaRepository repository;
  private final ModelMapper mapper;


  public ContaService(ContaRepository repository, ModelMapper mapper) {
    this.repository = repository;
    this.mapper = mapper;
  }

  public ContaResponse create(ContaRequest request) {
    Conta conta = mapper.map(request, Conta.class);
    conta.setStatus(true);
    conta = repository.save(conta);
    return mapper.map(conta, ContaResponse.class);
  }

  public ContaResponse findByCpf(String cpf) throws EntityNotFound {
    Conta conta = repository.findByCpf(cpf);
    if(Objects.isNull(conta)){
      throw new EntityNotFound();
    }
    return mapper.map(conta, ContaResponse.class);
  }

  public List<ContaResponse> findAll() {
    List<Conta> contas = repository.findAll();
        return contas.stream().filter(Conta::isStatus)
        .map(conta -> mapper.map(conta, ContaResponse.class))
        .collect(Collectors.toList());
  }

  public boolean softDelete(Integer id) throws EntityNotFound {
    findById(id);
    this.repository.softDeleteById(id);
    return true;
  }

  public ContaResponse update(Integer id, ContaRequest request) throws EntityNotFound {
    Conta conta = repository.findById(id).orElseThrow(EntityNotFound::new);
    Conta contaUpdate = mapper.map(request, Conta.class);
    contaUpdate.setId(id);
    if (!request.isStatus()) {
      conta.setStatus(false);
    }
    conta = repository.save(contaUpdate);
    return mapper.map(conta, ContaResponse.class);
  }


  public Conta findById(Integer id) throws EntityNotFound {
    return repository.findById(id).orElseThrow(EntityNotFound::new);
  }

}