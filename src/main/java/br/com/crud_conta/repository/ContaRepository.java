package br.com.crud_conta.repository;

import br.com.crud_conta.domain.Conta;

import java.util.List;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public interface ContaRepository extends CrudRepository<Conta, Integer> {

  List<Conta> findAll();

  @Query("SELECT c FROM Conta c WHERE c.cpf = :cpf and c.status = true")
  Conta findByCpf(@Param("cpf") String cpf);

  @Transactional
  @Modifying
  @Query("UPDATE Conta c SET c.status = false WHERE c.id = :id")
  void softDeleteById(@Param("id") Integer id);


}