package br.com.crud_conta.rest;

import br.com.crud_conta.domain.Conta;
import br.com.crud_conta.model.ContaRequest;
import br.com.crud_conta.model.ContaResponse;
import br.com.crud_conta.service.EntityNotFound;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.validation.Valid;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.crud_conta.service.ContaService;

@RestController
@RequestMapping(value = "/conta")
@Api(tags = "Conta", description = "API de criar conta")
public class ContaController {

  private ContaService service;


  public ContaController(ContaService service) {
    this.service = service;
  }

  @ApiOperation(value = "Busca todas as contas")
  @GetMapping
  public List<ContaResponse> findAll() {
    return service.findAll();
  }

  @ApiOperation(value = "Busca conta pelo CPF")
  @GetMapping(value = "/cpf/{cpf}")
  public ContaResponse findByCpf(@PathVariable("cpf") String cpf) throws EntityNotFound {
    return service.findByCpf(cpf);
  }

  @ApiOperation(value = "Busca conta pelo id")
  @GetMapping(value = "/{id}")
  public Conta findById(@PathVariable("id") Integer id) throws EntityNotFound {
    return service.findById(id);
  }

  @ApiOperation(value = "criar uma conta")
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public ContaResponse create(@RequestBody @Valid ContaRequest contaRequest) {
    return service.create(contaRequest);
  }

  @ApiOperation(value = "editar uma conta pelo id")
  @PutMapping(value = "/{id}")
  public ContaResponse update(@PathVariable("id") Integer id,
      @RequestBody @Valid ContaRequest contaRequest) throws EntityNotFound {
    return service.update(id, contaRequest);
  }

  @ApiOperation(value = "Deleta uma conta")
  @DeleteMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public boolean delete(@PathVariable("id") Integer id) throws EntityNotFound {
    return service.softDelete(id);
  }

}
