package br.com.crud_conta.domain;

import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Table(name = "conta")
public class Conta extends BaseDominio {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @NotNull
  @NotEmpty
  @Size(max = 6, message = "numero não pode ter mais de 6 caracteres")
  private String numero;

  @NotNull
  @NotEmpty
  @Size(max = 4, message = "agencia não pode ter mais de 4 caracteres")
  private String agencia;

  @NotNull
  @NotEmpty
  @CPF
  private String cpf;

  @NotNull
  @GeneratedValue(strategy = GenerationType.AUTO)
  private boolean status;

  @CreatedDate
  private LocalDate dataCriacao;

  @LastModifiedDate
  private LocalDate dataAtualizacao;

  public Conta() {}

  public Conta(String agencia, String numero, String cpf) {
    this.agencia = agencia;
    this.numero = numero;
    this.cpf = cpf;
  }

  public Conta(Integer id, String agencia, String numero, String cpf, boolean status) {
    this.id = id;
    this.agencia = agencia;
    this.numero = numero;
    this.cpf = cpf;
    this.status = status;
  }

  @ApiModelProperty(notes = "Id da conta")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @ApiModelProperty(notes = "número da conta")
  public String getNumero() {
    return numero;
  }

  public void setNumero(String numero) {
    this.numero = numero;
  }

  @ApiModelProperty(notes = "agência da conta")
  public String getAgencia() {
    return agencia;
  }

  public void setAgencia(String agencia) {
    this.agencia = agencia;
  }

  @ApiModelProperty(notes = "cpf cadastrado da conta")
  public String getCpf() {
    return cpf;
  }

  public void setCpf(String cpf) {
    this.cpf = cpf;
  }

  @ApiModelProperty(notes = "status da conta")
  public boolean isStatus() {
    return status;
  }

  public void setStatus(boolean status) {
    this.status = status;
  }

  @ApiModelProperty(notes = "data de criação da conta")
  public LocalDate getDataCriacao() {
    return dataCriacao;
  }

  public void setDataCriacao(LocalDate dataCriacao) {
    this.dataCriacao = dataCriacao;
  }

  @ApiModelProperty(notes = "data da última atualização")
  public LocalDate getDataAtualizacao() {
    return dataAtualizacao;
  }

  public void setDataAtualizacao(LocalDate dataAtualizacao) {
    this.dataAtualizacao = dataAtualizacao;
  }

}