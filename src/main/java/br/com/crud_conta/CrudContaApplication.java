package br.com.crud_conta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudContaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudContaApplication.class, args);
	}
	
}
