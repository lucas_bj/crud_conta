package br.com.crud_conta.model;

import java.time.LocalDate;

public class ContaResponse {

  private Integer id;
  private String numero;
  private String agencia;
  private String cpf;
  private boolean status;
  private LocalDate dataCriacao;
  private LocalDate dataAtualizacao;

  public ContaResponse(){}

  public ContaResponse(Integer id, String agencia, String numero, String cpf, LocalDate dataCriacao, LocalDate dataAtualizacao) {
    this.id = id;
    this.agencia = agencia;
    this.numero = numero;
    this.cpf = cpf;
    this.status = true;
    this.dataCriacao = dataCriacao;
    this.dataAtualizacao = dataAtualizacao;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getNumero() {
    return numero;
  }

  public void setNumero(String numero) {
    this.numero = numero;
  }

  public String getAgencia() {
    return agencia;
  }

  public void setAgencia(String agencia) {
    this.agencia = agencia;
  }

  public String getCpf() {
    return cpf;
  }

  public void setCpf(String cpf) {
    this.cpf = cpf;
  }

  public boolean isStatus() {
    return status;
  }

  public void setStatus(boolean status) {
    this.status = status;
  }

  public LocalDate getDataCriacao() {
    return dataCriacao;
  }

  public void setDataCriacao(LocalDate dataCriacao) {
    this.dataCriacao = dataCriacao;
  }

  public LocalDate getDataAtualizacao() {
    return dataAtualizacao;
  }

  public void setDataAtualizacao(LocalDate dataAtualizacao) {
    this.dataAtualizacao = dataAtualizacao;
  }


}
