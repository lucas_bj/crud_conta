package br.com.crud_conta.model;


public class ContaRequest {

  private String numero;
  private String agencia;
  private String cpf;
  private boolean status;

  public ContaRequest() {}

  public ContaRequest(String agencia, String numero, String cpf, Boolean status) {
    this.agencia = agencia;
    this.numero = numero;
    this.cpf = cpf;
    this.status = status;
  }

  public ContaRequest(String agencia, String numero, String cpf) {
    this.agencia = agencia;
    this.numero = numero;
    this.cpf = cpf;
  }

  public String getNumero() {
    return numero;
  }

  public void setNumero(String numero) {
    this.numero = numero;
  }

  public String getAgencia() {
    return agencia;
  }

  public void setAgencia(String agencia) {
    this.agencia = agencia;
  }

  public String getCpf() {
    return cpf;
  }

  public void setCpf(String cpf) {
    this.cpf = cpf;
  }

  public boolean isStatus() {
    return status;
  }

  public void setStatus(boolean status) {
    this.status = status;
  }


}
