package br.com.crud_conta.service;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.Matchers.is;

import br.com.crud_conta.domain.Conta;
import br.com.crud_conta.model.ContaRequest;
import br.com.crud_conta.model.ContaResponse;
import br.com.crud_conta.rest.ContaController;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
    ContaController.class
})
public class ContaControllerTest {

  private final String BASE_URL = "/conta";

  private ObjectMapper objectMapper;
  @Autowired
  private ContaController controller;
  private MockMvc mockMvc;

  @MockBean
  private ContaService mockService;


  @Before
  public void setUp() {
    objectMapper = new ObjectMapper();
    mockMvc = MockMvcBuilders
        .standaloneSetup(controller)
        .build();
  }

  @Test
  public void deveBuscarId15ERetornar200() throws Exception {

    Conta conta = new Conta(15, "0100", "351121", "02721739085", true);

    when(mockService.findById(15)).thenReturn(conta);

    mockMvc.perform(get(BASE_URL + "/15"))
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id", is(15)))
        .andExpect(jsonPath("$.agencia", is("0100")))
        .andExpect(jsonPath("$.numero", is("351121")))
        .andExpect(jsonPath("$.cpf", is("02721739085")))
        .andExpect(jsonPath("$.status", is(true)));

    verify(mockService, times(1)).findById(15);
  }

  @Test
  public void deveBuscarIdInvalidoERetornar404() throws Exception {
    when(mockService.findById(2)).thenThrow(new EntityNotFound());

    mockMvc.perform(get(BASE_URL + "/2"))
        .andDo(print())
        .andExpect(status().isNotFound());
  }

  @Test
  public void deveBuscarCPFERetornar200() throws Exception {

    ContaResponse response = new ContaResponse(15, "0100", "351121", "02721739085", LocalDate.of(2019, 12, 10),
        LocalDate.now());

    when(mockService.findByCpf("02721739085")).thenReturn(response);

    mockMvc.perform(get(BASE_URL + "/cpf/02721739085"))
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk())
        .andDo(print())
        .andExpect(jsonPath("$.id", is(15)))
        .andExpect(jsonPath("$.agencia", is("0100")))
        .andExpect(jsonPath("$.numero", is("351121")))
        .andExpect(jsonPath("$.cpf", is("02721739085")))
        .andExpect(jsonPath("$.status", is(true)));

    verify(mockService, times(1)).findByCpf("02721739085");
  }

  @Test
  public void deveBuscarCpfInvalidoERetornar404() throws Exception {
    when(mockService.findByCpf("4")).thenThrow(new EntityNotFound());

    mockMvc.perform(get(BASE_URL + "/cpf/4"))
        .andDo(print())
        .andExpect(status().isNotFound());
  }

  @Test
  public void deveCriarNovaContaERetornar201() throws Exception {

    ContaRequest request = new ContaRequest("0100", "351121", "02721739085");

    ContaResponse response = new ContaResponse(15, "0100", "351121", "02721739085", LocalDate.of(2019, 12, 10),
        LocalDate.now());

    when(mockService.create(any(ContaRequest.class))).thenReturn(response);

    mockMvc.perform(post(BASE_URL)
        .content(objectMapper.writeValueAsString(request))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.id", is(15)))
        .andExpect(jsonPath("$.agencia", is("0100")))
        .andExpect(jsonPath("$.numero", is("351121")))
        .andExpect(jsonPath("$.cpf", is("02721739085")));

    verify(mockService, times(1)).create(any(ContaRequest.class));

  }

  @Test
  public void deveAtualizarUmaContaERetornar_200() throws Exception {

    ContaRequest request = new ContaRequest("0100", "351121", "02721739085", true);

    Conta conta = new Conta(13, "0100", "351121", "02721739080", true);

    ContaResponse response = new ContaResponse(13, "0100", "351121", "02721739085", LocalDate.of(2019, 12, 10),
        LocalDate.now());

    when(mockService.findById(13)).thenReturn(conta);
    when(mockService.update(eq(13), any(ContaRequest.class))).thenReturn(response);

    mockMvc.perform(get(BASE_URL + "/13"))
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id", is(13)))
        .andExpect(jsonPath("$.cpf", is("02721739080")));

    verify(mockService, times(1)).findById(13);

    mockMvc.perform(put(BASE_URL + "/13")
        .content(objectMapper.writeValueAsString(request))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andDo(print())
        .andExpect(jsonPath("$.id", is(13)))
        .andExpect(jsonPath("$.agencia", is("0100")))
        .andExpect(jsonPath("$.numero", is("351121")))
        .andExpect(jsonPath("$.cpf", is("02721739085")));

    verify(mockService, times(1)).update(eq(13), any(ContaRequest.class));
  }

  @Test
  public void DeveRealizarDeleteLogicoERetornar204() throws Exception {

    Conta conta = new Conta(13, "0100", "351121", "02721739080", false);

    when(mockService.findById(13)).thenReturn(conta);
    when(mockService.softDelete(13)).thenReturn(true);

    mockMvc.perform(delete(BASE_URL + "/13"))
        .andDo(print())
        .andExpect(status().isNoContent())
        .andExpect(jsonPath("$", is(true)));

    verify(mockService, times(1)).softDelete(13);

    mockMvc.perform(get(BASE_URL + "/13"))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id", is(13)))
        .andExpect(jsonPath("$.status", is(false)));

    verify(mockService, times(1)).findById(13);
  }

}
